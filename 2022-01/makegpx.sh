#! /bin/bash
BASEDIR=$(basename "$(pwd)")
if test -f Van-$BASEDIR.gpx; then
  rm Van-$BASEDIR.gpx
fi
cat 09*.GPX > Van-$BASEDIR.gpx && vim -c ":%s/<\/trkseg>\_.\{-}<trkseg>//g" -c ":%s/<ele>\_.\{-}<\/speed>//g" -c ":wq" Van-$BASEDIR.gpx
# if you need to process more than one do like `cat 01*.GPX 02*.GPX > Van-...`
